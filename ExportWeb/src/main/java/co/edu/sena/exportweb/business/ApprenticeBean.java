/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.exportweb.business;

import co.edu.sena.exportweb.model.Apprentice;
import co.edu.sena.exportweb.model.Course;
import co.edu.sena.exportweb.model.LateArrival;
import co.edu.sena.exportweb.persistence.IApprenticeDAO;
import co.edu.sena.exportweb.utils.Constants;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.TextAlignment;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.math3.analysis.function.Constant;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class ApprenticeBean implements ApprenticeBeanLocal {

    private File temporalFile;

    @EJB
    private IApprenticeDAO apprenticeDAO;

    @Override
    public Apprentice findById(Long document) throws Exception {
        if (document == 0) {
            throw new Exception("El documento es obligatorio");
        }

        return apprenticeDAO.findById(document);
    }

    @Override
    public List<Apprentice> findAll() throws Exception {
        return apprenticeDAO.findAll();
    }

    @Override
    public void exportPDFApprentices(OutputStream outputStream) throws Exception {
        PdfWriter pdfWriter = new PdfWriter(outputStream);
        PdfDocument pdfDocument = new PdfDocument(pdfWriter);
        Document document = new Document(pdfDocument);

        Paragraph paragraph = new Paragraph("Reporte de aprendices");
        paragraph.setFontSize(14);
        paragraph.setTextAlignment(TextAlignment.CENTER);
        paragraph.setBold();
        paragraph.setFontColor(ColorConstants.ORANGE);
        document.add(paragraph);
        document.add(new Paragraph("")); //Salto de linea

        float[] columnsWidths = {150F, 150F, 150F};
        Table table = new Table(columnsWidths);
        table.addCell(new Cell().add(new Paragraph("Documento")));
        table.addCell(new Cell().add(new Paragraph("Nombre")));
        table.addCell(new Cell().add(new Paragraph("Programa")));

        List<Apprentice> apprentices = findAll();
        for (Apprentice apprentice : apprentices) {
            table.addCell(new Cell().add(new Paragraph(String.valueOf(apprentice.getDocument()))));
            table.addCell(new Cell().add(new Paragraph(apprentice.getFullName())));
            table.addCell(new Cell().add(new Paragraph(apprentice.getIdCourse().getCareer())));
        }

        document.add(table);
        document.close();
        pdfWriter.close();
        outputStream.close();;
        System.out.println("PDF creado");
    }

    @Override
    public void sendEmail(String to, String subject) throws Exception {
        Properties props = new Properties();
        props.put("mail.transport.protocol", Constants.PROTOCOL);
        props.put("mail.smtp.host", Constants.HOST);
        props.put("mail.smtp.socketFactory.port", Constants.SOCKET_FACTORY_PORT);
        props.put("mail.smtp.socketFactory.class", Constants.SOCKET_FACTORY);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", Constants.PORT);
        props.put("mail.smtp.user", Constants.USER);
        props.put("mail.smtp.password", Constants.PASSWORD);
        props.put("mail.smtp.starttls.enable", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);
        String bodyMessage = setMessage();

        InternetAddress from = new InternetAddress(props.getProperty("mail.smtp.user"));
        message.setFrom(from); //Remitente
        message.setSubject(subject); //Asunto
        message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to)); //Destinantario
        message.setContent(bodyMessage, "text/html; charset=utf-8"); //Cuerpo mensaje

        Transport transport = session.getTransport(Constants.PROTOCOL);
        transport.connect(props.getProperty("mail.smtp.host"), props.getProperty("mail.smtp.user"),
                props.getProperty("mail.smtp.password"));
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();

    }

    public String setMessage() throws Exception {
        String message = "";
        try {
            List<Apprentice> apprentices = findAll();
            message = "<p>Cordial saludo, se envia el reporte de aprendices registrados en ARLET</p>"
                    + "<table border='1'>"
                    + "<tr><th>Documento</th><th>Nombre</th><th>Programa</th></tr>";
            for (Apprentice apprentice : apprentices) {
                message += "<tr>"
                        + "<td>" + apprentice.getDocument() + "</td>"
                        + "<td>" + apprentice.getFullName() + "</td>"
                        + "<td>" + apprentice.getIdCourse().getCareer() + "</td>"
                        + "</tr>";
            }

            message += "</table>";
        } catch (Exception e) {
            throw e;
        }
        return message;
    }

    @Override
    public void sendEmailAttachment(String to, String subject, Long Document) throws Exception {
        Properties props = new Properties();
        props.put("mail.transport.protocol", Constants.PROTOCOL);
        props.put("mail.smtp.host", Constants.HOST);
        props.put("mail.smtp.socketFactory.port", Constants.SOCKET_FACTORY_PORT);
        props.put("mail.smtp.socketFactory.class", Constants.SOCKET_FACTORY);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", Constants.PORT);
        props.put("mail.smtp.user", Constants.USER);
        props.put("mail.smtp.password", Constants.PASSWORD);
        props.put("mail.smtp.starttls.enable", "true");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);
        String bodyMessage = setMessage();

        exportExcelTemp(Document);

        MimeBodyPart text = new MimeBodyPart();
        text.setContent(bodyMessage, "text/html; charset=utf-8");
        //Crear archivo adjunto de correo
        MimeBodyPart attach = new MimeBodyPart();
        DataHandler dh = new DataHandler(new FileDataSource(temporalFile));
        attach.setDataHandler(dh);
        attach.setFileName(dh.getName());
        //Crear un contenedor del mensaje con las dos partes
        MimeMultipart mp = new MimeMultipart();
        mp.addBodyPart(text);
        mp.addBodyPart(attach);
        mp.setSubType("mixed");

        InternetAddress from = new InternetAddress(props.getProperty("mail.smtp.user"));
        message.setFrom(from); //Remitente
        message.setSubject(subject); //Asunto
        message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to)); //Destinantario
        message.setContent(mp); //Cuerpo mensaje

        Transport transport = session.getTransport(Constants.PROTOCOL);
        transport.connect(props.getProperty("mail.smtp.host"), props.getProperty("mail.smtp.user"),
                props.getProperty("mail.smtp.password"));
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
        temporalFile.deleteOnExit();
    }

    public void exportExcelTemp(Long Document) throws Exception {
        Apprentice apprentice = findById(Document);
        List<LateArrival> lateArrivalList = (List<LateArrival>) apprentice.getLateArrivalCollection();

        temporalFile = File.createTempFile("late_arrivals", ".xlsx");
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Llegadas tarde");
        //Estilo para el encabezado
        CellStyle styleHeader = workbook.createCellStyle();
        styleHeader.setFillForegroundColor(IndexedColors.INDIGO.getIndex());
        styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleHeader.setBorderTop(BorderStyle.THIN);
        styleHeader.setBorderRight(BorderStyle.THIN);
        styleHeader.setBorderLeft(BorderStyle.THIN);
        styleHeader.setBorderBottom(BorderStyle.THIN);

        String[] titles = {"Id", "Fecha", "Observaciones", "Documento aprendiz", "Nombre Aprendiz"};
        Row row = sheet.createRow(0);
        //Creamos el encabezado
        for (int i = 0; i < titles.length; i++) {
            org.apache.poi.ss.usermodel.Cell cell = row.createCell(i);
            cell.setCellStyle(styleHeader);
            cell.setCellValue(titles[i]);
        }

        //Estilo para celdas fecha
        CreationHelper creationHelper = workbook.getCreationHelper();
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("yyyy-MM-dd"));

        //Colocamos los datos en las siguientes filas
        for (int i = 0; i < lateArrivalList.size(); i++) {
            row = sheet.createRow(i+1);
            row.createCell(0).setCellValue(lateArrivalList.get(i).getId());
            org.apache.poi.ss.usermodel.Cell dateArrivalCell = row.createCell(1);
            dateArrivalCell.setCellStyle(dateCellStyle);
            dateArrivalCell.setCellValue(lateArrivalList.get(i).getDateArrival());
            row.createCell(2).setCellValue(lateArrivalList.get(i).getObservations());
            row.createCell(3).setCellValue(lateArrivalList.get(i).getDocumentApprentice().getDocument());
            row.createCell(4).setCellValue(lateArrivalList.get(i).getDocumentApprentice().getFullName());
        }

        FileOutputStream outputStream = new FileOutputStream(temporalFile);
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();
        System.out.println("Archivo late arrival excel creado");
    }
}
