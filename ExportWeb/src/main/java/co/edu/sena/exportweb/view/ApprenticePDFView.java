/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package co.edu.sena.exportweb.view;

import co.edu.sena.exportweb.business.ApprenticeBeanLocal;
import co.edu.sena.exportweb.utils.MessageUtils;
import java.io.IOException;
import java.io.OutputStream;
import javax.ejb.EJB;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.commons.compress.utils.CountingOutputStream;

/**
 *
 * @author Aprendiz
 */
public class ApprenticePDFView {
    
    @EJB
    private ApprenticeBeanLocal apprenticeBean;

    /**
     * Creates a new instance of ApprenticePDFView
     */
    public ApprenticePDFView() {
    }
    
    public void exportPDF(){
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        ec.responseReset();
        ec.setResponseHeader("Content-Type", "application/pdf");
        ec.setResponseHeader("Content-Disposition", "attachment;filename=\"apprentices.pdf\"");
        try(OutputStream output = ec.getResponseOutputStream()) {
            apprenticeBean.exportPDFApprentices(output);
            //Obtener el tamaño del archivo
            try(CountingOutputStream co = new CountingOutputStream(output)) {
                ec.setResponseContentLength( (int) co.getBytesWritten());
                co.close();
            } catch (IOException e) {
                MessageUtils.addErrorMessage("Error al obtener el tamaño del archivo " + e.getMessage());
            }
        } catch (Exception e) {
            MessageUtils.addErrorMessage("Error al crear el Excel " + e.getMessage());
        }
        fc.responseComplete();
    }
    
}
