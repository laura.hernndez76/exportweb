/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package co.edu.sena.exportweb.utils;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * Fecha: 13/09/2022
 * @author Aprendiz
 * Objetivo: clase de utilería para mensajes en pantalla
 */
public class MessageUtils {
    
    public static void addErrorMessage(String message)
    {
        FacesContext.getCurrentInstance().addMessage(null, 
            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", message));
    }
    
    public static void addWarningMessage(String message)
    {
        FacesContext.getCurrentInstance().addMessage(null, 
            new FacesMessage(FacesMessage.SEVERITY_WARN, "Advertencia", message));
    }
    
    public static void addInfoMessage(String message)
    {
        FacesContext.getCurrentInstance().addMessage(null, 
            new FacesMessage(FacesMessage.SEVERITY_INFO, "Mensaje", message));
    }
    
}
