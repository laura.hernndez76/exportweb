/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSF/JSFManagedBean.java to edit this template
 */
package co.edu.sena.exportweb.view;

import co.edu.sena.exportweb.business.ApprenticeBeanLocal;
import co.edu.sena.exportweb.utils.MessageUtils;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.primefaces.component.inputtext.InputText;

/**
 *
 * @author Aprendiz
 */
@Named(value = "apprenticeEmailView")
@RequestScoped
public class ApprenticeEmailView {

    private InputText txtEmail;
    private InputText txtSubject;
    private InputText txtDocument;

    @EJB
    private ApprenticeBeanLocal apprenticeBean;

    /**
     * Creates a new instance of ApprendiceEmailView
     */
    public ApprenticeEmailView() {
    }

    public InputText getTxtDocument() {
        return txtDocument;
    }

    public void setTxtDocument(InputText txtDocument) {
        this.txtDocument = txtDocument;
    }

    public InputText getTxtEmail() {
        return txtEmail;
    }

    public void setTxtEmail(InputText txtEmail) {
        this.txtEmail = txtEmail;
    }

    public InputText getTxtSubject() {
        return txtSubject;
    }

    public void setTxtSubject(InputText txtSubject) {
        this.txtSubject = txtSubject;
    }

    public void sendEmail() {
        try {
            apprenticeBean.sendEmail(txtEmail.getValue().toString(), txtSubject.getValue().toString());
            MessageUtils.addInfoMessage("Mensaje enviado al correo electronico");
        } catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }

    public void sendEmailAttachment() {
        try {
            apprenticeBean.sendEmailAttachment(txtEmail.getValue().toString(), txtSubject.getValue().toString(),
                    Long.parseLong(txtDocument.getValue().toString()));
        } catch (Exception e) {
            MessageUtils.addErrorMessage(e.getMessage());
        }
    }

}
